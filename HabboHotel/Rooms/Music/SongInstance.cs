﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

using Calln.HabboHotel.GameClients;
using Calln.HabboHotel.Rooms;
using Calln.HabboHotel.Users;
using Calln.Communication.Packets.Incoming;
using System.Collections.Concurrent;

using Calln.Database.Interfaces;
using log4net;
using Calln.HabboHotel.Items;

namespace Calln.HabboHotel.Rooms.Music
{
    public class SongInstance
    {
        private readonly SongItem mDiskItem;
        private readonly SongData mSongData;

        public SongInstance(SongItem Item, SongData SongData)
        {
            mDiskItem = Item;
            mSongData = SongData;
        }

        public SongData SongData
        {
            get { return mSongData; }
        }

        public SongItem DiskItem
        {
            get { return mDiskItem; }
        }
    }
}