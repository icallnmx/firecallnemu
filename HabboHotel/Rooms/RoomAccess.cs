﻿namespace Calln.HabboHotel.Rooms
{
    public enum RoomAccess
    {
        OPEN,
        DOORBELL,
        PASSWORD,
        INVISIBLE
    }
}
