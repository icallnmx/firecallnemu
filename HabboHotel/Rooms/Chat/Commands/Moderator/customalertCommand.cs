﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Calln.Communication.Packets.Outgoing.Moderation;
using Calln.Communication.Packets.Outgoing.Rooms.Notifications;

namespace Calln.HabboHotel.Rooms.Chat.Commands.Moderator
{
    class customalertCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "user_11"; }
        }

        public string Parameters
        {
            get { return "[MENSAJE]"; }
        }

        public string Description
        {
            get { return "Envia un mensaje de alerta al Hotel."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Params.Length == 1)
            {
                Session.SendWhisper("Por favor, escriba un mensaje a enviar.", 34);
                return;
            }

            string Message = CommandManager.MergeParams(Params, 1);

            CallnEnvironment.GetGame().GetClientManager().SendMessage(new RoomCustomizedAlertComposer(Message));



            return;
        }
    }
}
