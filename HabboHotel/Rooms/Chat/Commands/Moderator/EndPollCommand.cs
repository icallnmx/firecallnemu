﻿using Calln.HabboHotel.GameClients;
using Calln.HabboHotel.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Calln.HabboHotel.Rooms.Chat.Commands.Moderator
{
    internal class EndPollCommand : IChatCommand
    {

        public void Execute(GameClient Session, Room Room, string[] Params)
        {
           
                    Room.endQuestion();
        }

        public string Description =>
            "Borrar una encuesta rápida.";

        public string Parameters =>
            "";

        public string PermissionRequired =>
            "user_6";
    }
}