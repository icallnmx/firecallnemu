﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;

using Calln.Communication.Packets.Outgoing.Users;
using Calln.Communication.Packets.Outgoing.Notifications;


using Calln.Communication.Packets.Outgoing.Handshake;
using Calln.Communication.Packets.Outgoing.Quests;
using Calln.HabboHotel.Items;
using Calln.Communication.Packets.Outgoing.Inventory.Furni;
using Calln.Communication.Packets.Outgoing.Catalog;
using Calln.HabboHotel.Quests;
using Calln.HabboHotel.Rooms;
using System.Threading;
using Calln.HabboHotel.GameClients;
using Calln.Communication.Packets.Outgoing.Rooms.Avatar;
using Calln.Communication.Packets.Outgoing.Pets;
using Calln.Communication.Packets.Outgoing.Messenger;
using Calln.HabboHotel.Users.Messenger;
using Calln.Communication.Packets.Outgoing.Rooms.Polls;
using Calln.Communication.Packets.Outgoing.Rooms.Notifications;
using Calln.Communication.Packets.Outgoing.Availability;
using Calln.Communication.Packets.Outgoing;

namespace Calln.HabboHotel.Rooms.Chat.Commands.Administrator
{
    class FacebookCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "user_13"; }
        }

        public string Parameters
        {
            get { return "[DE Q SE TRATA]"; }
        }

        public string Description
        {
            get { return "¡¡Nuevo concurso de fb!!"; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Params.Length == 2)
            {
                Session.SendWhisper("Porfavor introduce el mensaje y el link para enviarlo..");
                return;
            }

            string URL = "https://www.facebook.com/jabbozz/";

            string Message = CommandManager.MergeParams(Params, 1);
            Session.SendMessage(new RoomNotificationComposer("Hay un nuevo concurso en Facebook",
                 "   ¿De qué se trata?\n\n\n" +
                 "  <font color=\"#a62984\"><b>" + Message +
                 "  <br><br></b></font><b>El concurso de Facebook es realizado por: </b> <b><font color=\"#224CAD\">" + Session.GetHabbo().Username +
                 "  <br><br></b></font></b><b>Hora actual:</b> " + DateTime.Now + "\n\n" +
                 "Para acceder a la página de Facebook haz clic en Ir al Facebook", "habbo_talent_show_stage", "Ir al facebook >>", URL));
            return;

        }
    }
}
