﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;

using Calln.Communication.Packets.Outgoing.Users;
using Calln.Communication.Packets.Outgoing.Notifications;


using Calln.Communication.Packets.Outgoing.Handshake;
using Calln.Communication.Packets.Outgoing.Quests;
using Calln.HabboHotel.Items;
using Calln.Communication.Packets.Outgoing.Inventory.Furni;
using Calln.Communication.Packets.Outgoing.Catalog;
using Calln.HabboHotel.Quests;
using Calln.HabboHotel.Rooms;
using System.Threading;
using Calln.HabboHotel.GameClients;
using Calln.Communication.Packets.Outgoing.Rooms.Avatar;
using Calln.Communication.Packets.Outgoing.Pets;
using Calln.Communication.Packets.Outgoing.Messenger;
using Calln.HabboHotel.Users.Messenger;
using Calln.Communication.Packets.Outgoing.Rooms.Polls;
using Calln.Communication.Packets.Outgoing.Rooms.Notifications;
using Calln.Communication.Packets.Outgoing.Availability;
using Calln.Communication.Packets.Outgoing;
using Calln.Communication.Packets.Outgoing.Nux;

namespace Calln.HabboHotel.Rooms.Chat.Commands.User
{
    class ColourList : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_info"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "Información de Calln."; }
        }

        public void Execute(GameClient Session, Room Room, string[] Params)
        {
            Session.SendMessage(new RoomNotificationComposer("Lista de colores:",
                 "<font color='#FF8000'><b>COLORES:</b>\n" +
                 "<font size=\"12\" color=\"#1C1C1C\">El comando :color te permitirá fijar un color que tu desees en tu bocadillo de chat, para poder seleccionar el color deberás especificarlo después de hacer el comando, como por ejemplo:<br><i>:color red</i></font>" +
                 "<font size =\"13\" color=\"#0B4C5F\"><b>Stats:</b></font>\n" +
                 "<font size =\"11\" color=\"#1C1C1C\">  <b> · Users: </b> \r  <b> · Rooms: </b> \r  <b> · Uptime: </b>minutes.</font>\n\n" +
                 "", "quantum", ""));
        }
    }
}