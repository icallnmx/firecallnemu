﻿using Calln.HabboHotel.GameClients;

using Calln.Communication.Packets.Outgoing.Rooms.Notifications;
using Calln.Database.Interfaces;
using System.Data;
using System;
using Calln.Communication.Packets.Outgoing.Rooms.Engine;

namespace Calln.HabboHotel.Rooms.Chat.Commands.User
{
    class ViewVIPStatusCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_info"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "Información de tu suscripción VIP."; }
        }

        public void Execute(GameClient Session, Room Room, string[] Params)
        {
            Session.SendMessage(RoomNotificationComposer.SendBubble("abuse", "No eres miembro del Club VIP de Havvo, haz click aquí para abonarte.", ""));
        }
    }
}