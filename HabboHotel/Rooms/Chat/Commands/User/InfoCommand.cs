﻿using System;
using Calln.HabboHotel.GameClients;
using Calln.Communication.Packets.Outgoing.Rooms.Notifications;
using Calln.Communication.Packets.Outgoing.Catalog;
using Calln.Communication.Packets.Outgoing;
using Calln.Communication.Packets.Outgoing.Misc;
using Calln.Communication.Packets.Outgoing.Rooms.Freeze;
using Calln.Communication.Packets.Outgoing.Rooms.Settings;
using System.Text;
using Calln.Communication.Packets.Outgoing.Handshake;
using Calln.HabboHotel.Users;
using Calln.Communication.Packets.Outgoing.Help.Helpers;
using Calln.HabboHotel.Moderation;
using Calln.Utilities;
using System.Collections.Generic;
using Calln.Database.Interfaces;
using System.Data;

namespace Calln.HabboHotel.Rooms.Chat.Commands.User
{
    class InfoCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "user_normal"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "Información de Calln."; }
        }

        public void Execute(GameClient Session, Room Room, string[] Params)
        {
            TimeSpan Uptime = DateTime.Now - CallnEnvironment.ServerStarted;
            int OnlineUsers = CallnEnvironment.GetGame().GetClientManager().Count;
            int RoomCount = CallnEnvironment.GetGame().GetRoomManager().Count;
            DataRow Items = null, rooms = null, users = null;

            using (IQueryAdapter dbClient = CallnEnvironment.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("SELECT count(id) FROM users");
                users = dbClient.getRow();
                dbClient.SetQuery("SELECT count(id) FROM users WHERE DATE(FROM_UNIXTIME(account_created)) = CURDATE()");
                Items = dbClient.getRow();
                dbClient.SetQuery("SELECT count(id) FROM rooms");
                rooms = dbClient.getRow();
            }
            Session.SendMessage(new RoomNotificationComposer("Información del servidor",
                "<font color='#0D0106'><b>FireCalln Server:</b>\n" +
                "<font size=\"11\" color=\"#1C1C1C\">FireCalln Server es un emulador potente, creativo y divertido, </font>" +
                "<font size=\"11\" color=\"#1C1C1C\">su principal función es confirmar lo mencionado antes y ser original. \n\n      ¡FireCalln es un proyecto a código abierto!\n\n" +
                "<font size =\"12\" color=\"#FF5100\"><b>Información:</b></font>\n" +
                "<font size =\"11\" color=\"#1C1C1C\">  <b> · Usuarios conectados</b>: " + OnlineUsers + "\r" +
                "  <b> · Salas en linea</b>: " + RoomCount + "\r" +
                "  <b> · Tiempo en linea</b>: " + Uptime.Days + " days and " + Uptime.Hours + " hours.\r" +
                "  <b> · Fecha de hoy</b>: " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt") + ".\n\n" +
                "<font size =\"12\" color=\"#0B4C5F\"><b>Estadísticas</b></font>\n" +
                "  <b> · Usuarios conectados</b>: " + Game.SessionUserRecord + "\r" +
                "  <b> · Usuarios registrados</b>: " + users[0] + "\r" +
                "  <b> · Registrados este día</b>: " + Items[0] + "\r" +
                "  <b> · Salas creadas</b>:  " + rooms[0] + ".</font>\n\n" +
                "<font size =\"12\" color=\"#0077E7\"><b>Contacto y Soporte:</b></font>\n" +
                "<font size =\"11\" color=\"#1C1C1C\">  <b> · CallnMx</b>: instagram.com/callnaaron" + "\r" +
                "  <b> · Firewall</b>: instagram.com/huertas56" + "\r" +
                "                          Creado por:  <b>Calln y Firewall</b>.\n\n", "whited"));
        }
    }
}