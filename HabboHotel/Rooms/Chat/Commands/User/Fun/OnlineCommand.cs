﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calln.Communication.Packets.Outgoing.Rooms.Chat;
using Calln.HabboHotel.GameClients;

namespace Calln.HabboHotel.Rooms.Chat.Commands.User.Fun
{
    class OnlineCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "user_normal"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "Ver cantidad de usuarios en línea."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            int OnlineUsers = CallnEnvironment.GetGame().GetClientManager().Count;

            Session.SendWhisper("Ahora mismo hay " + OnlineUsers + " usuarios conectados en "+CallnEnvironment.HotelName+" :).", 34);
        }
    }
}

