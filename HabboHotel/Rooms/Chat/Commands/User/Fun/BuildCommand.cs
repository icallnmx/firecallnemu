﻿using Calln.Communication.Packets.Outgoing.Inventory.Furni;
using Calln.Communication.Packets.Outgoing.Rooms.Engine;
using Calln.Communication.Packets.Outgoing.Rooms.Furni;
using Calln.Database.Interfaces;
using Calln.HabboHotel.GameClients;
using Calln.HabboHotel.Items;
using Calln.HabboHotel.Rooms;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Calln.HabboHotel.Rooms.Chat.Commands.User.Fan
{
    internal class BuildCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_give"; }
        }

        public string Parameters
        {
            get { return "%height%"; }
        }

        public string Description
        {
            get { return ""; }
        }
        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            string height = Params[1];
            if (Session.GetHabbo().Id == Room.OwnerId)
            {
                if (!Room.CheckRights(Session, true))
                    return;
                Item[] items = Room.GetRoomItemHandler().GetFloor.ToArray();
                foreach (Item Item in items.ToList())
                {
                    GameClient TargetClient = CallnEnvironment.GetGame().GetClientManager().GetClientByUserID(Item.UserID);
                    if (Item.GetBaseItem().InteractionType == InteractionType.STACKTOOL)

                        Room.SendMessage(new UpdateMagicTileComposer(Item.Id, int.Parse(height)));
                }
            }
        }
    }
}