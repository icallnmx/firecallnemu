﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;

using Calln.Communication.Packets.Outgoing.Users;
using Calln.Communication.Packets.Outgoing.Notifications;


using Calln.Communication.Packets.Outgoing.Handshake;
using Calln.Communication.Packets.Outgoing.Quests;
using Calln.HabboHotel.Items;
using Calln.Communication.Packets.Outgoing.Inventory.Furni;
using Calln.Communication.Packets.Outgoing.Catalog;
using Calln.HabboHotel.Quests;
using Calln.HabboHotel.Rooms;
using System.Threading;
using Calln.HabboHotel.GameClients;
using Calln.Communication.Packets.Outgoing.Rooms.Avatar;
using Calln.Communication.Packets.Outgoing.Pets;
using Calln.Communication.Packets.Outgoing.Messenger;
using Calln.HabboHotel.Users.Messenger;
using Calln.Communication.Packets.Outgoing.Rooms.Polls;
using Calln.Communication.Packets.Outgoing.Rooms.Notifications;
using Calln.Communication.Packets.Outgoing.Availability;
using Calln.Communication.Packets.Outgoing;
using Calln.Communication.Packets.Outgoing.Nux;

namespace Calln.HabboHotel.Rooms.Chat.Commands.User
{
    class PriceList : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_info"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "Ver la lista de precios de raros."; }
        }

        public void Execute(GameClient Session, Room Room, string[] Params)
        {
            StringBuilder List = new StringBuilder("");
            List.AppendLine("                          ¥ LISTA DE PRECIOS DE HABBI¥");
            List.AppendLine("   SOFÁ VIP: Duckets   »   SOFÁ VIP: Duckets   »   SOFÁ VIP: Duckets");
            List.AppendLine("   SOFÁ VIP: Duckets   »   SOFÁ VIP: Duckets   »   SOFÁ VIP: Duckets");
            List.AppendLine("   SOFÁ VIP: Duckets   »   SOFÁ VIP: Duckets   »   SOFÁ VIP: Duckets");
            List.AppendLine("   SOFÁ VIP: Duckets   »   SOFÁ VIP: Duckets   »   SOFÁ VIP: Duckets");
            List.AppendLine("   SOFÁ VIP: Duckets   »   SOFÁ VIP: Duckets   »   SOFÁ VIP: Duckets");
            List.AppendLine("   SOFÁ VIP: Duckets   »   SOFÁ VIP: Duckets   »   SOFÁ VIP: Duckets");
            List.AppendLine("Esta lista todavía está en construcción por Custom, su última actualización fue el día 28 de Julio de 2016.");
            Session.SendMessage(new MOTDNotificationComposer(List.ToString()));


        }
    }
}