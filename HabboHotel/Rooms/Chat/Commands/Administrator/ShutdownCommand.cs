﻿using System;
using System.Linq;
using Calln.Database.Interfaces;
using System.Data;
using Calln.Communication.Packets.Outgoing.Rooms.Notifications;

namespace Calln.HabboHotel.Rooms.Chat.Commands.User
{
    class ShutdownCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "user_16"; }
        }
        public string Parameters
        {
            get { return ""; }
        }
        public string Description
        {
            get { return "¡Cierra el hotel!"; }
        }
        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {

            CallnEnvironment.GetGame().GetClientManager().SendMessage(new RoomCustomizedAlertComposer(CallnEnvironment.HotelName + " será cerrado en pocos segundos.\n\n - " + Session.GetHabbo().Username + ""));
            Session.SendWhisper("Se cerrara el hotel en un minuto!", 34);
            CallnEnvironment.PerformShutDown();
        }
    }
}