﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;

using Calln.Communication.Packets.Outgoing.Users;
using Calln.Communication.Packets.Outgoing.Notifications;


using Calln.Communication.Packets.Outgoing.Handshake;
using Calln.Communication.Packets.Outgoing.Quests;
using Calln.HabboHotel.Items;
using Calln.Communication.Packets.Outgoing.Inventory.Furni;
using Calln.Communication.Packets.Outgoing.Catalog;
using Calln.HabboHotel.Quests;
using Calln.HabboHotel.Rooms;
using System.Threading;
using Calln.HabboHotel.GameClients;
using Calln.Communication.Packets.Outgoing.Rooms.Avatar;
using Calln.Communication.Packets.Outgoing.Pets;
using Calln.Communication.Packets.Outgoing.Messenger;
using Calln.HabboHotel.Users.Messenger;
using Calln.Communication.Packets.Outgoing.Rooms.Polls;
using Calln.Communication.Packets.Outgoing.Rooms.Notifications;
using Calln.Communication.Packets.Outgoing.Availability;
using Calln.Communication.Packets.Outgoing;


namespace Calln.HabboHotel.Rooms.Chat.Commands.Events
{
    internal class SpecialEvent : IChatCommand
    {
        public string PermissionRequired
        {
            get
            {
                return "user_13";
            }
        }
        public string Parameters
        {
            get { return "[EXPLICACION]"; }
        }
        public string Description
        {
            get
            {
                return "Manda un evento a todo el hotel.";
            }
        }
        public void Execute(GameClient Session, Room Room, string[] Params)
        {
            if (Params.Length == 1)
            {
                Session.SendWhisper("Por favor, digite un mensaje para enviar.", 34);
                return;
            }
            else
            {
                string Message = CommandManager.MergeParams(Params, 1);

                CallnEnvironment.GetGame().GetClientManager().SendMessage(new RoomNotificationComposer("¿Qué está pasando en " + CallnEnvironment.HotelName + "...?",
                     Message, "event_image", "¡A la aventura!", "event:navigator/goto/" + Session.GetHabbo().CurrentRoomId));
            }

        }
    }
}

