﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace Calln.HabboHotel.Rooms.Chat.Emotions
{
    enum ChatEmotions
    {
        Smile,
        Angry,
        Sad,
        Shocked,
        None
    }
}
