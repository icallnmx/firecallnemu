﻿using System.Reflection;
using System.Runtime.InteropServices;
using Calln;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("CallnMX Emulator")]
[assembly: AssemblyDescription("CallnMx Emulator, Editor Forbi")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("CallnMx")]
[assembly: AssemblyProduct("CallnMx Emulator")]
[assembly: AssemblyCopyright("Copyright © CallnMx Comapny 2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.

[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM

[assembly: Guid("7781d8c0-4f30-4462-8a50-e47244b2d386")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]

[assembly: AssemblyVersion(CallnEnvironment.PrettyBuild)]
[assembly: AssemblyFileVersion(CallnEnvironment.PrettyBuild)]
