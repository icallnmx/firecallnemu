﻿namespace Calln.Communication.Interfaces
{
    public interface IServerPacket
    {
        byte[] GetBytes();
    }
}