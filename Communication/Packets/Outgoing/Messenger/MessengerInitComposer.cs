﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Calln.HabboHotel.Users;
using Calln.HabboHotel.Users.Messenger;
using Calln.HabboHotel.Users.Relationships;

namespace Calln.Communication.Packets.Outgoing.Messenger
{
    class MessengerInitComposer : ServerPacket
    {
        public MessengerInitComposer()
            : base(ServerPacketHeader.MessengerInitMessageComposer)
        {
            base.WriteInteger(CallnStaticGameSettings.MessengerFriendLimit);//Friends max.
            base.WriteInteger(300);
            base.WriteInteger(800);
            base.WriteInteger(1); // category count
            base.WriteInteger(1);//category id
            base.WriteString("Grupos");//category name
        }
    }
}
