﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Calln.HabboHotel.Items.Crafting;

namespace Calln.Communication.Packets.Outgoing.Rooms.Furni
{
    class MysticBoxCloseComposer : ServerPacket
    {
        public MysticBoxCloseComposer()
            : base(ServerPacketHeader.MysticBoxCloseComposer)
        {
        }
    }
}