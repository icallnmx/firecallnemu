﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Calln.HabboHotel.Items.Crafting;

namespace Calln.Communication.Packets.Outgoing.Rooms.Furni
{
    class MysticBoxRewardComposer : ServerPacket
    {
        public MysticBoxRewardComposer(string type, int itemID)
            : base(ServerPacketHeader.MysticBoxRewardComposer)
        {
            base.WriteString(type);
            base.WriteInteger(itemID);
        }
    }
}