﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Calln.HabboHotel.Rooms;
using Calln.Communication.Packets.Outgoing.Rooms.Session;
using Calln.Communication.Packets.Outgoing.Nux;

namespace Calln.Communication.Packets.Incoming.Navigator
{
    class FindRandomFriendingRoomEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            var type = Packet.PopString();
            if (type == "predefined_noob_lobby")
            {
                Session.SendMessage(new NuxAlertComposer("nux/lobbyoffer/hide"));
                Session.SendMessage(new RoomForwardComposer(4));
                return;
            }

            Room Instance = CallnEnvironment.GetGame().GetRoomManager().TryGetRandomLoadedRoom();
            if (Instance != null) Session.SendMessage(new RoomForwardComposer(Instance.Id));
        }
    }
}
