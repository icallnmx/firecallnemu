﻿using System;
using Calln.Communication.Packets.Incoming;
using Calln.HabboHotel.GameClients;
using Calln.Communication.Packets.Outgoing.Users;
using Calln.Communication.Packets.Outgoing.Handshake;
using Calln.HabboHotel.Rooms;

namespace Calln.Communication.Packets.Incoming.Users
{
    class ScrGetUserInfoEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            Session.SendMessage(new ScrSendUserInfoComposer(Session.GetHabbo()));
            Session.SendMessage(new UserRightsComposer(Session.GetHabbo()));

        }
    }
}
