﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Calln.Communication.Packets.Outgoing.Groups;

namespace Calln.Communication.Packets.Incoming.Groups
{
    class GetBadgeEditorPartsEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            Session.SendMessage(new BadgeEditorPartsComposer(
                CallnEnvironment.GetGame().GetGroupManager().Bases,
                CallnEnvironment.GetGame().GetGroupManager().Symbols,
                CallnEnvironment.GetGame().GetGroupManager().BaseColours,
                CallnEnvironment.GetGame().GetGroupManager().SymbolColours,
                CallnEnvironment.GetGame().GetGroupManager().BackGroundColours));

        }
    }
}
