﻿using Calln.HabboHotel.GameClients;
using Calln.Communication.Packets.Incoming;

namespace Calln.Communication.Packets.Incoming.Handshake
{
    public class GetClientVersionEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            string Build = Packet.PopString();

            if (CallnEnvironment.SWFRevision != Build)
                CallnEnvironment.SWFRevision = Build;
        }
    }
}