﻿using System;
using Calln.Database.Interfaces;
using Calln.HabboHotel.GameClients;
using Calln.Communication.Packets.Outgoing.Handshake;

namespace Calln.Communication.Packets.Incoming.Handshake
{
    public class UniqueIDEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            string Junk = Packet.PopString();
            string MachineId = Packet.PopString();

            Session.MachineId = MachineId;

            Session.SendMessage(new SetUniqueIdComposer(MachineId));
        }
    }
}