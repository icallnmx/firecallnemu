﻿using Calln.Communication.Packets.Outgoing.Catalog;
using Calln.HabboHotel.GameClients;
using Calln.Communication.Packets.Incoming;

namespace Calln.Communication.Packets.Incoming.Catalog
{
    public class GetGiftWrappingConfigurationEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            Session.SendMessage(new GiftWrappingConfigurationComposer());
        }
    }
}