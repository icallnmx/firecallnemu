﻿using System.Collections.Generic;
using Calln.HabboHotel.GameClients;
using Calln.HabboHotel.Quests;
using Calln.Communication.Packets.Incoming;

namespace Calln.Communication.Packets.Incoming.Quests
{
    public class GetQuestListEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            CallnEnvironment.GetGame().GetQuestManager().GetList(Session, null);
        }
    }
}