﻿using System;
using Calln.HabboHotel.GameClients;
using Calln.Communication.Packets.Outgoing.Inventory.Purse;
using Calln.Utilities;
using Calln.HabboHotel.Items;
using Calln.Communication.Packets.Outgoing.Inventory.Furni;
using Calln.Communication.Packets.Outgoing.Rooms.Notifications;
using Calln.Communication.Packets.Outgoing.Rooms.Nux;

namespace Calln.Communication.Packets.Incoming.Rooms.Nux
{
    class NuxAcceptGiftsMessageEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            Session.SendMessage(new NuxItemListComposer());
        }
    }
}