﻿using System;
using System.Linq;
using System.Text;

using Calln.Communication.Packets.Incoming;
using Calln.HabboHotel.GameClients;
using Calln.Communication.Packets.Outgoing.Rooms.Session;
using Calln.Communication.Packets.Outgoing.Rooms.Engine;
using Calln.Communication.Packets.Outgoing.Nux;
using Calln.HabboHotel.Rooms;
using Calln.HabboHotel.Users;

namespace Calln.Communication.Packets.Incoming.Rooms.Connection
{
    class GoToFlatAsSpectatorEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            if (!Session.GetHabbo().InRoom)
                return;

            //Session.GetHabbo().Spectating = true;
            //Session.SendMessage(new RoomSpectatorComposer());

            //Room roomToSpec = Session.GetHabbo().CurrentRoom;
            
            //roomToSpec.QueueingUsers.Remove(Session.GetHabbo());
            //foreach (Habbo user in roomToSpec.QueueingUsers)
            //{
            //    if (roomToSpec.QueueingUsers.First().Id == user.Id)
            //    {
            //        user.PrepareRoom(roomToSpec.Id, "");
            //    }
            //    else
            //    {
            //        user.GetClient().SendMessage(new RoomQueueComposer(roomToSpec.QueueingUsers.IndexOf(user)));
            //    }
            //}
            
            if (!Session.GetHabbo().EnterRoom(Session.GetHabbo().CurrentRoom))
                Session.SendMessage(new CloseConnectionComposer());
        }
    }
}
