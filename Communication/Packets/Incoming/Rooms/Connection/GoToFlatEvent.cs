﻿using System;
using System.Linq;
using System.Text;

using Calln.Communication.Packets.Incoming;
using Calln.HabboHotel.GameClients;
using Calln.Communication.Packets.Outgoing.Rooms.Session;
using Calln.Communication.Packets.Outgoing;
using Calln.Communication.Packets.Outgoing.Nux;

namespace Calln.Communication.Packets.Incoming.Rooms.Connection
{
    class GoToFlatEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            if (!Session.GetHabbo().InRoom)
                return;

            if (!Session.GetHabbo().EnterRoom(Session.GetHabbo().CurrentRoom))
                Session.SendMessage(new CloseConnectionComposer());
        }
    }
}
