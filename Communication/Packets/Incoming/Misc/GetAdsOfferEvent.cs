﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Calln.HabboHotel.Users;
using Calln.Communication.Packets.Outgoing.Rooms.Notifications;
using Calln.HabboHotel.Rooms;
using Calln.HabboHotel.GameClients;
using Calln.Communication.Packets.Outgoing.Handshake;

namespace Calln.Communication.Packets.Incoming.Misc
{
    class GetAdsOfferEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            Session.SendMessage(new VideoOffersRewardsComposer());
        }
    }
}
