﻿using Calln.Communication.Packets.Incoming;
using Calln.HabboHotel.GameClients;

namespace Calln.Communication.Packets
{
    public interface IPacketEvent
    {
        void Parse(GameClient Session, ClientPacket Packet);
    }
}